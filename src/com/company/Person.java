package com.company;

/**
 * Created by RENT on 2017-08-16.
 */
public class Person implements Comparable<Person>{

    private String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Person o) {
//        return age - o.age;

        int i = surname.compareTo(o.surname);

        if (i == 0){
            return name.compareTo(o.name);
        }else {
            return i;
        }

    }
}
